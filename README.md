# myTheme #

This is a light and dark html/js/css theme to use in my projects.

To use this theme the following need to be inserted in the head element of the each page that will use the theme:

``` html
<script type="text/javascript" src="theme-selector.js"></script>
```
.

There is also the possibility to use a toggle button for switching between light and dark theme: 
``` html
<div class="theme-selector small ts-right"></div>
```

Demo:  https://elvizakos.gitlab.io/mytheme/
