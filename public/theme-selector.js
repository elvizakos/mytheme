if ( typeof tstheme == 'undefined' ) {
	var tstheme = null;
	// var tstheme = 'light';
	// var tstheme = 'dark';

	tstheme = getCookie('theme');
}
function switchTheme ( e ) {

	document.body.classList.remove('ts-'+tstheme);
	tstheme = tstheme == 'dark' ? 'light' : 'dark';
	document.body.classList.add('ts-'+tstheme);

	setCookie('theme',tstheme);

	if ( tstheme == 'dark' ) {
		darkl.disabled = false;
		lightl.disabled = true;
	}
	else if ( tstheme == 'light' ) {
		darkl.disabled = true;
		lightl.disabled = false;
	}
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

var selfscript = document.getElementsByTagName('script');
selfscript = selfscript[selfscript.length - 1];
var selfsrc = selfscript.src;
selfsrc = selfsrc.substr(0,selfsrc.lastIndexOf('/')+1);

var mainl = document.createElement('link');
mainl.rel = 'stylesheet';
mainl.href = selfsrc+'main.css';
mainl.type = 'text/css';
mainl.media = 'screen';
// document.head.appendChild(mainl);
selfscript.parentNode.insertBefore(mainl, selfscript);

var darkl = document.createElement('link');
darkl.rel = 'stylesheet';
darkl.href = selfsrc+'dark.css';
darkl.type = 'text/css';
darkl.media = 'screen';
darkl.disabled = true;
// document.head.appendChild(darkl);
selfscript.parentNode.insertBefore(darkl, selfscript);

var lightl = document.createElement('link');
lightl.rel = 'stylesheet';
lightl.href = selfsrc+'light.css';
lightl.type = 'text/css';
lightl.media = 'screen';
lightl.disabled = true;
// document.head.appendChild(lightl);
selfscript.parentNode.insertBefore(lightl, selfscript);

window.addEventListener('load', function () {

	if ( tstheme === null ) {
		if ( document.body.classList.contains('ts-light') ) tstheme = 'light';
		else if ( document.body.classList.contains('ts-dark') ) tstheme = 'dark';
	}
	if ( tstheme === null ) {
		if ( window.matchMedia('(prefers-color-scheme: dark)').matches === true ) {
			lightl.disabled = true;
			darkl.disabled = false;
			tstheme = 'dark';
			document.body.classList.add('ts-'+tstheme);
		}
		else if ( window.matchMedia('(prefers-color-scheme: light)').matches === true ) {
			lightl.disabled = false;
			darkl.disabled = true;
			tstheme = 'light';
			document.body.classList.add('ts-'+tstheme);
		}

		window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', e => {
			if ( e.matches === true ) {
				darkl.disabled = false;
				tstheme = 'dark';
				document.body.classList.add('ts-'+tstheme);
			}
			else {
				darkl.disabled = true;
			}
		});

		window.matchMedia('(prefers-color-scheme: light)').addEventListener('change', e => {
			if ( e.matches === true ) {
				lightl.disabled = false;
				tstheme = 'light';
				document.body.classList.add('ts-'+tstheme);
			}
			else {
				lightl.disabled = true;
			}
		});
	}
	else {
		document.body.classList.add('ts-'+tstheme);
		if ( tstheme == 'dark' ) {
			darkl.disabled = false;
			lightl.disabled = true;
		}
		else if ( tstheme == 'light' ) {
			darkl.disabled = true;
			lightl.disabled = false;
		}
	}

	// Load theme selector
	var tsb = document.getElementsByClassName('theme-selector');
	var el = {};
	for ( var i=0;i<tsb.length;i++) {

		if ( !tsb[i].title ) tsb[i].title = "Switch theme";
		tsb[i].addEventListener('click', switchTheme);

		el.bullet = document.createElement('div');
		el.bullet.classList.add('ts-bullet');
		tsb[i].appendChild(el.bullet);

		// Put selector on the right
		if (tsb[i].classList.contains('ts-right')) {
			tsb[i].style.cssFloat = 'right';
		}

	}

});
